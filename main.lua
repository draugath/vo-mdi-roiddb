declare("roiddb", {
	notesid = 764332,
	mdi_id = "roiddb",
	sort_dir = -1,
	distances = {},
	cur_target = {},
	timer = Timer(),
	has_mdi = false,
})
local ore_reference


local function ImportTargetlessData()
	local tldata = unspickle(LoadSystemNotes(741937))
	for sectorid, v in pairs(tldata) do
		roiddb.OreList[sectorid] = roiddb.OreList[sectorid] or {}
		local sectordata = unspickle(v)
		for oid, roiddata in pairs(sectordata) do
			roiddb.OreList[sectorid][oid] = roiddb.OreList[sectorid][oid] or {}
			if (not next(roiddb.OreList[sectorid][oid])) then 
				for ore, v in pairs(roiddata.ore) do
					roiddb.OreList[sectorid][oid][ore] = tonumber(v)
				end
			end
		end
	end
	SaveSystemNotes(spickle(roiddb.OreList), roiddb.notesid)
	roiddb.roidlistctl:setuplist()
end

local function IsDeclared(value)
	return pcall(loadstring("return "..value))
end

local function count_unscanned()
	local count = 0
	for id, data in pairs(roiddb.OreList[roiddb.cur_sector]) do
		if (not next(data)) then count = count + 1 end
	end
	return count
end

local function count_scanned()
	local count = 0
	for id, data in pairs(roiddb.OreList[roiddb.cur_sector]) do
		if (next(data)) then count = count + 1 end
	end
	return count
end

local function SetRoidTarget(nid, oid, cb, reset)
	local success = false
	local cb_result
	local old_nid, old_oid = radar.GetRadarSelectionID()
	radar.SetRadarSelection(nid, oid)
	local new_nid, new_oid = radar.GetRadarSelectionID()
	if (new_nid == nid and new_oid == oid) then success = true end
	if (success and type(cb) == "function") then cb_result = cb() end
	if (reset) then radar.SetRadarSelection(old_nid, old_oid) end

	return success, cb_result
end

local function GetRoidDistance(oid, subdlg)
	subdlg = subdlg or roiddb.roidlistctl.Roids[oid]
	if (not subdlg) then return end
	
	local success, distance = SetRoidTarget(
		roiddb.OreList[roiddb.cur_sector].node or 2,
		oid,
		function()
			local distance = GetTargetDistance()
			if (distance) then distance = math.floor(distance) end
			return distance
		end,
		true -- reset to current target
	)
	return distance
end

local function CycleRoids()
	for oid, subdlg in pairs(roiddb.roidlistctl.Roids) do
		if (type(oid) ~= "number") then break end
		local distance = GetRoidDistance(oid, subdlg)
		subdlg[1][1][15].title = distance or ""
		roiddb.distances[oid] = distance	
	end
	
	roiddb.timer:SetTimeout(1000, CycleRoids)
end

local function active_ping_action()
	roiddb.active_ping = not roiddb.active_ping
	if (roiddb.active_ping) then
		CycleRoids()
	else
		roiddb.timer:Kill()
		for _, subdlg in ipairs(roiddb.roidlistctl.Items) do
			if (tonumber(subdlg[1][1][1].title)) then
				subdlg[1][1][15].title = ""
			end
		end
		roiddb.distances = {}
	end
end

local function show_unscanned_action()
	roiddb.show_unscanned = not roiddb.show_unscanned
	roiddb.roidlistctl:setuplist()
end

function roiddb:CreateContent()
	local pdlg, main
	local roidlistctl
	local distance_label, header_width_label
	local roids_scanned, roids_max_estimate
	local active_ping, show_unscanned
	local cancel_button
	
	local id_size = 40
	local ore_size = 36
	local distance_size
	
	local function default_sort_func(a, b)
		if (roiddb.sort_dir > 0) then
			return a > b
		else
			return a < b
		end
	end
	local sort_func = default_sort_func

	local function sortlist(self)
		-- generic sort_button action
		local new_sort_func = ore_reference[4][self.title] or default_sort_func
		if (sort_func == new_sort_func) then roiddb.sort_dir = -roiddb.sort_dir end
		sort_func = new_sort_func
		roidlistctl:setuplist() 
	end
	
	local function makesubdlg(content, oid)
		local subdlg, canvas

		subdlg = iup.dialog{
			iup.zbox{
				content,
				canvas,
				all = "YES",
			},
			border="NO",menubox="NO",resize="NO",
			fullscreen="NO",
			bgcolor = "0 0 0 0 *",
			expand = "NO",
		}

		return subdlg
	end

	-- ### Content Definition ###
	-- Header with sort buttons
	local sort_buttons = {margin = "2x", gap = 3, alignment = "ABOTTOM", iup.stationbutton{title = "ID", size = id_size + 1, action = sortlist}}
	for _, ore in ipairs(ore_reference[1]) do
		local sort_button = iup.stationbutton{title = ore_reference[2][ore], size = ore_size, tip = ore, action = sortlist}
		table.insert(sort_buttons, sort_button)
	end
	distance_label = iup.label{title = "Distance"}
	table.insert(sort_buttons, distance_label)
	
	-- This label is only used to determine the width of the header for resizing the listbox.  It is removed in post-processing.
	header_width_label = iup.label{title = "width", expand = "HORIZONTAL", bgcolor = "0 255 0 127 *"}

	-- Listbox, duh
	roidlistctl = iup.stationsublist{
		expand = "YES",
		control = "YES",
		border = "NO",
		Items = {}, -- list items in index order
		Roids = {}, -- list items by object id
		size = 'x250',
	}

		
	-- footer options
	if (not roiddb.has_mdi) then
		active_ping = iup.stationtoggle{title = "Active Ping", value = roiddb.active_ping and "ON" or "OFF", action = active_ping_action}
		show_unscanned = iup.stationtoggle{title = "Show Unscanned", value = roiddb.show_unscanned and "ON" or "OFF", action = show_unscanned_action}
		cancel_button = iup.stationbutton{title = "", size = 1, visible = "NO", action = function(self) HideDialog(iup.GetDialog(self)) end}
	end
	roids_scanned = iup.label{title = "0"}
	roids_max_estimate = iup.label{title = "0"}
	
	main = iup.hudrightframe{
		iup.vbox{
			iup.hbox(sort_buttons),
			header_width_label,
			roidlistctl,
			iup.hbox{iup.hbox{active_ping, show_unscanned, cancel_button, gap = 10}, iup.fill{}, iup.hbox{roids_scanned, iup.label{title = " / "}, roids_max_estimate}},
			margin = "1x1",
		},
	}

	if (not roiddb.has_mdi) then
		roiddb.dlg = iup.dialog{
			iup.pdarootframe{
				main,
			},
			defaultesc = cancel_button,
			topmost = "YES",
			title = "Roid DB",
			border = "NO",
			bgcolor = "0 0 0 0 *",
		}
		roiddb.dlg:map()
	end

	sort_buttons = nil
	
	-- ### Callbacks ###
	
	function roidlistctl:action(t, i, v)
		local oid = tonumber(self.Items[i].oid)
		if (v ~= 1 or not oid) then return end
		
		local old_nid, old_oid = radar.GetRadarSelectionID()
		radar.SetRadarSelection(roiddb.OreList[roiddb.cur_sector].node or 2, oid)
		local new_nid, new_oid = radar.GetRadarSelectionID()
		if (not new_oid or (oid ~= new_oid)) then HUD:PrintSecondaryMsg("Asteroid is out of range.") end
	end
	
	function roidlistctl:clearlist()
		if self[1] then
			self[1] = nil
			for i in ipairs(self.Items) do
				self.Items[i]:destroy()
				self.Items[i] = nil
			end
			self.Roids = {}
		end
	end

	function roidlistctl:additem(oid, i)
		i = i or #self.Items + 1
		local ores = {iup.label{title = oid, size = id_size, expand = "HORIZONTAL", wordwrap = "NO"}, gap = 3, expand = "YES"}

		if (type(oid) == "number") then
			local roid = roiddb.OreList[roiddb.cur_sector][oid]
			for _, ore in ipairs(ore_reference[1]) do
				local label = iup.label{title = roid[ore] or "", size = ore_size, wordwrap = "NO", expand = "NO", alignment = "ACENTER"}
				table.insert(ores, label)
			end
			table.insert(ores, iup.label{title = roiddb.distances[oid] or "", size = distance_size, alignment = "ARIGHT"})
		end
		local subdlg = makesubdlg(iup.hbox(ores), oid)
		subdlg.bgcolor = ListColors[i%2].." 128 *"
		subdlg.oid = oid
		self.Items[i] = subdlg
		self.Roids[oid] = subdlg
		iup.Append(self, subdlg)
	end
		
	
	function roidlistctl:setuplist()
		if (roiddb.has_mdi) then mdi.dont_hide = true end
		self:clearlist()
		
		local max_est = 0

		if (not distance_size) then distance_size = tonumber(distance_label.size:match("(%d+)x")) end
		local roids = roiddb.OreList[roiddb.cur_sector] or {}
		local sorted_list = {}
		for id in pairs(roids) do
			if (type(id) == "number" and (next(roids[id]) or roiddb.show_unscanned)) then 
				max_est = math.max(max_est, id)
				table.insert(sorted_list, id)
			end
		end
		table.sort(sorted_list, sort_func)
		if (#sorted_list > 0) then
			for i, oid in ipairs(sorted_list) do
				roidlistctl:additem(oid, i)
			end
		else
			self:additem("No asteroids have been scanned in this sector.")
		end
		
		roids_scanned.title = #sorted_list
		roids_max_estimate.title = max_est
		
		self:map()
		self[1] = 1
		if (roiddb.has_mdi) then mdi.dont_hide = false end
	end
	
	function roidlistctl:remap_list()
		-- Needed in the MDI environment to remove horizontal scrollbars displayed after z-order shift
		self[1] = nil
		self:map()
		self[1] = 1
	end

	function main:show_cb()
		if (not IsConnected()) then return end
		roidlistctl:setuplist()
	end
	

	local function post_process(pdlg)
		-- listbox size is being set based on the width of the widest element in the pdialog
		-- and is not being adjusted when wider contents are added.  Use this to size the listbox
		roidlistctl.size = (main[1][2].size:match("(%d+)x") + Font.Default + 4).."x"..roidlistctl.size:match("x(%d+)")
		iup.Detach(header_width_label)
		iup.Destroy(header_width_label)
		iup.Refresh(roidlistctl)
	end
	
	if (not roiddb.has_mdi) then post_process() end
	roiddb.roidlistctl = roidlistctl
	
	return main, post_process
end

local function refresh_display(e) -- TODO rename and/or move this
	if (roiddb.has_mdi) then
		if (e == "MDI_rHUDxscale_FINISHED" or e == "PLAYER_ENTERED_GAME") then 
			roiddb.pdlg = mdi:GetWindow(roiddb.mdi_id)
		end
		roiddb.pdlg.contents[1]:show_cb()
	else
		roiddb.dlg[1][1]:show_cb()
	end
end	

function roiddb:TARGET_SCANNED(_, minerals, nodeid, objectid)
	local roid_list = self.OreList[self.cur_sector] or {}
	local dirty = false
	roid_list.node = nodeid
	if (minerals == "Object too far to scan\n") then
		if (not roid_list[objectid]) then
			roid_list[objectid] = {}
			dirty = true
		end
		
	elseif (minerals:match("Minerals:")) then
		if (not roid_list[objectid]) then 
			roid_list[objectid] = {}
			dirty = true
		end

		minerals:gsub("(%w+) Ore: ([%d.]+)%%", function(o, p)
			p = tonumber(p)
			if (roid_list[objectid][o] ~= p) then
				roid_list[objectid][o] = p
				dirty = true
			end
		end)
	end
	if (dirty) then 
		SaveSystemNotes(spickle(self.OreList), self.notesid)
		self.OreList[self.cur_sector] = roid_list
		refresh_display()
	end
end
RegisterEvent(roiddb, "TARGET_SCANNED")


RegisterEvent(function(_, sectorid)
	roiddb.cur_sector = sectorid
	
	refresh_display()
end, "SECTOR_CHANGED")

RegisterEvent(function(e)
	roiddb.OreList = unspickle(LoadSystemNotes(roiddb.notesid))
	roiddb.cur_sector = GetCurrentSectorid()
	refresh_display(e)
end, "PLAYER_ENTERED_GAME")

RegisterEvent(function(_, id)
	if (id == roiddb.mdi_id) then roiddb.roidlistctl:remap_list() end
end, "MDI_ZORDER_SHIFT")

RegisterEvent(refresh_display, "MDI_rHUDxscale_FINISHED")
	

RegisterEvent(function()
	if (IsDeclared("mdi")) then roiddb.has_mdi = true end
	
	if (roiddb.has_mdi) then
	
		local context_menu_items = {
			{
				title = "Active Ping",
				type = "toggle",
				value = function() return roiddb.active_ping and "ON" or "OFF" end,
				action = active_ping_action,
			},
			{
				title = "Show Unscanned",
				type = "toggle",
				value = function() return roiddb.show_unscanned and "ON" or "OFF" end,
				action = show_unscanned_action,
			},
			{
				title = "SEPARATOR",
			},
			{
				title = "Advanced...",
				action = {
					{
						title = "Import Targetless Data",
						action = ImportTargetlessData,
					}
				},
			},
		}

		mdi:CreateWindow(
			roiddb.mdi_id,
			roiddb.CreateContent,
			{title = "Roid DB", cx = 300, cy = 300},
			context_menu_items
		)
		
	else
		roiddb:CreateContent()
		RegisterUserCommand("roiddb", function(_, args) 
			if (args and args[1] == 'import') then
				ImportTargetlessData()
			else
				ShowDialog(roiddb.dlg, iup.CENTER, iup.CENTER) 
			end
		end)
	end
end, "PLUGINS_LOADED")

ore_reference = {
	[1] = {
		'Aquean',
		'Silicate',
		'Carbonic',
		'Ferric',
		'Ishik',
		'VanAzek',
		'Xithricite',
		'Lanthanic',
		'Denic',
		'Pyronic',
		'Apicene',
		'Pentric',
		'Heliocene',
	},
	[2] = {
		Aquean     = 'Aq',
		Silicate   = 'Si',
		Carbonic   = 'Ca',
		Ferric     = 'Fe',
		Ishik      = 'Is',
		VanAzek    = 'Va',
		Xithricite = 'Xi',
		Lanthanic  = 'La',
		Denic      = 'De',
		Pyronic    = 'Py',
		Apicene    = 'Ap',
		Pentric    = 'Pe',
		Heliocene  = 'He',
	},
	[3] = {
		Aq = 'Aquean',
		Si = 'Silicate',
		Ca = 'Carbonic',
		Fe = 'Ferric',
		Is = 'Ishik',
		Va = 'VanAzek',
		Xi = 'Xithricite',
		La = 'Lanthanic',
		De = 'Denic',
		Py = 'Pyronic',
		Ap = 'Apicene',
		Pe = 'Pentric',
		He = 'Heliocene',
	},
	[4] = {},
}
for abbr, long in pairs(ore_reference[3]) do
	ore_reference[4][abbr] = function(a, b) 
		local roids = roiddb.OreList[roiddb.cur_sector]
		local roid_a = roids[a] or {}
		local roid_b = roids[b] or {}
		if (roiddb.sort_dir > 0) then
			return (roid_a[long] or 0) > (roid_b[long] or 0)
		else
			return (roid_a[long] or 0) < (roid_b[long] or 0)
		end
	end
end


--[[

Event: TARGET_SCANNED - Data:
        1: (string)     "Minerals:
Aquean Ore: 43.3%
Silicate Ore: 42.2%
Ishik Ore: 14.6%
"
        2: (number)     2
        3: (number)     57


Event: TARGET_SCANNED - Data:
        1: (string)     "Object too far to scan
"
        2: (number)     2
        3: (number)     48

Event: SECTOR_CHANGED - Data:
        1: (number)     3317 (Jallik E-16)

	
]]


--[[
Code to highlight a list row when moused over -- doesn't allow the scrollbar to work
	canvas = mdi.CreateControl(iup.canvas, {border = "NO"})
	canvas = iup.canvas{border = "NO"}
	function canvas:button_cb(b, s, x, y, f)
		if (s == 0) then
			local old_nid, old_oid = radar.GetRadarSelectionID()
			radar.SetRadarSelection(roiddb.OreList[roiddb.cur_sector] or 2, oid)
			local new_nid, new_oid = radar.GetRadarSelectionID()
			if (old_oid == new_oid) then HUD:PrintSecondaryMsg("Asteroid is out of range.") end
		end
	end
	
	function canvas:enterwindow_cb()
		if (not self.already_ran) then
			self.orig_color = subdlg.bgcolor
			subdlg.bgcolor = ListColors[2]
		end
		self.already_ran = not self.already_ran
	end

	function canvas:leavewindow_cb()
		if (not self.already_ran) then
			subdlg.bgcolor = self.orig_color
			self.orig_color = nil
		end
		self.already_ran = not self.already_ran
	end
]]